% Default to the notebook output style
((* if not cell_style is defined *))
    ((* set cell_style = 'custom_template_style_ipython.tpl' *))
((* endif *))

% Inherit from the specified cell style.
((* extends cell_style *))


%===============================================================================
% Latex Article
%===============================================================================

((* block docclass *))
\documentclass[../relazione/main.tex]{subfiles}
((* endblock docclass *))