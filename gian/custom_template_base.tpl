((= Latex base template (must inherit)
This template builds upon the abstract template, adding common latex output
functions.  Figures, data_text,
This template does not define a docclass, the inheriting class must define this.=))

((*- extends 'custom_document_contents.tpl' -*))

%===============================================================================
% Abstract overrides
%===============================================================================

((* block header *))
    ((* block docclass *))((* endblock docclass *))
    
((* endblock header *))

((* block body *))
    \begin{document}
    
    ((* block predoc *))
    ((* block maketitle *))((* endblock maketitle *))
    ((* block abstract *))((* endblock abstract *))
    ((* endblock predoc *))

    ((( super() )))

    % Add a bibliography block to the postdoc
    ((* block postdoc *))
    ((* block bibliography *))((* endblock bibliography *))
    ((* endblock postdoc *))
    \end{document}
((* endblock body *))