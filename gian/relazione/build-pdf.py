import os
import multiprocessing as mp

esfolder = [folder for folder in os.listdir('../') if folder.startswith('es')]
print(esfolder)
command = '''
jupyter nbconvert es{}.ipynb 
    --TagRemovePreprocessor.remove_cell_tags={\"remove_cell\"} 
    --TagRemovePreprocessor.remove_input_tags={\"remove_input\"} 
    --template=../custom_template_article.tpl 
    --to latex
'''
with mp.Pool(mp.cpu_count()) as p:
