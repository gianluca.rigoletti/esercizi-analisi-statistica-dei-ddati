from timeit import default_timer as timer
import numpy as np
import matplotlib.pyplot as plt
import itertools
from multiprocessing import Pool


def knapsack_bruteforce(W, weights, values):
    data = list(zip(weights, values))
    totweights = []
    totvalues = []
    for i in range(1, len(values) + 1):
        for combinations in itertools.combinations(data, i):
            combinations = np.array(combinations)
            totweight = combinations[:, 0].sum()
            totvalue = combinations[:, 1].sum()
            totweights.append(totweight)
            totvalues.append(totvalue)

    totweights = np.array(totweights)
    totvalues = np.array(totvalues)
    maxvalue = totvalues[totweights <= W].max()
    return maxvalue


def knapsack(W, weights, values):
    if W == 0 or len(weights) == 0:
        return 0
    if weights[-1] > W:  # l'oggetto non può essere incluso
        return knapsack(W, weights[:-1], values[:-1])
    else:
        # case1: ultimo oggetto incluso
        # case2: ultimo oggetto escluso
        case1 = values[-1] + \
            knapsack(W - weights[-1], weights[:-1], values[:-1])
        case2 = knapsack(W, weights[:-1], values[:-1])
        return max(case1, case2)


def knapsack_lookup(W, weights, values):
    # utilizzo una tabella e la riempio con
    # un approccio bottom up
    table = np.zeros((len(weights) + 1, W + 1))
    for i in range(1, len(weights) + 1):
        for w in range(1, W + 1):
            if weights[-1] <= w:
                case1 = values[i - 1] + table[i - 1, w - weights[i - 1]]
                case2 = table[i - 1, w]
                table[i, w] = max(case1, case2)
            else:
                table[i, w] = table[i - 1, w]
    return table[len(weights), W]


def test_knapsack(func, filename, sizes):
    W = 400
    np.random.seed(1000)
    times = []
    for size in sizes:
        values = np.random.randint(1, 400, size=size)
        weights = np.random.randint(1, 400, size=size)
        start = timer()
        func(W, weights, values)
        stop = timer()
        times.append(stop - start)
        print('Computed problem with {} elements'.format(size))
        with open(filename, 'a') as file:
            file.write('{}\t{}\n'.format(size, stop - start))

    plt.plot(sizes, times, 'o--')
    plt.show()


# p = Pool(2)
# args = zip(
#     [knapsack, knapsack_lookup],
#     ['times_recursive.txt', 'times_lookup.txt']
# )
# p.starmap(test_knapsack, args)
sizes = np.array([3, 5, 8, 10, 15, 16, 18, 20, 22, 24, 26, 28, 30, 50,
                  60, 70, 75, 82, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100
                  ],  dtype=np.int16)
test_knapsack(knapsack, 'times_recursive.txt', sizes)
# test_knapsack(knapsack_bruteforce, 'times_bruteforce.txt')
# test_knapsack(knapsack, 'times_recursive.txt')
# test_knapsack(knapsack_lookup, 'times_lookup.txt')
# sizes, times = np.genfromtxt('times_raw.txt', unpack=True)
# plt.plot(sizes, times, 'o--')
# plt.xlabel('N items')
# plt.ylabel('Time (seconds)')
# plt.title('Computational time of knapsack problem $2^N$')
# plt.grid(linestyle='--')
# plt.yscale('log')
# plt.show()
