import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

results_bruteforce = pd.read_csv(
    'times_bruteforce.txt', header=None, sep='\t', names=['items', 'time'])
results_lookup = pd.read_csv(
    'times_lookup.txt', header=None, sep='\t', names=['items', 'time'])
results_recursive = pd.read_csv(
    'times_recursive.txt', header=None, sep='\t', names=['items', 'time'])

plt.figure(figsize=(8, 5), dpi=150)
plt.plot(results_bruteforce['items'],
         results_bruteforce['time'], '.--', label='Brute force')
plt.plot(results_lookup['items'], results_lookup['time'],
         '.--', label='Look up table')
plt.plot(results_recursive['items'],
         results_recursive['time'], '.--', label='Naive recursive')
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.xlabel('Items')
plt.ylabel('Time [s]')
plt.grid(linestyle='--')
plt.savefig('results.pdf')
plt.savefig('results.png')
plt.show()
