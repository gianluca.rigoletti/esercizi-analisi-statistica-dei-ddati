import numpy as np


def find_minimum(func, x1=None, N=200, params=np.array([1., 0.5, 2., 0.5])):
    alpha, beta, gamma, delta = params
    vfunc = np.vectorize(func)
    x2, x3 = np.zeros((2, 2))
    if x1 is None:  # costruzione simplesso iniziale
        x1 = np.array([1., 0])
    x2, x3 = construct_simplex(x1)
    simplex = np.array([x1, x2, x3])
    # simplexes = simplex

    for i in range(N):
        # simplexes = np.append(simplexes, simplex, axis=0)
        points = vfunc(simplex[:, 0], simplex[:, 1])
        osimplex = simplex[np.argsort(points)]  # ordered simplex
        c = centroid(osimplex)
        best, second, worst = osimplex
        xr = reflection(worst, c, alpha)
        fxr = func(*xr)
        if func(*best) < fxr < func(*second):  # caso 1: rimpiazzare xworst con xr
            simplex[np.where((simplex == worst).all(axis=1))] = xr
        elif fxr < func(*best):  # caso migliore: ulteriore espansione
            xe = expansion(xr, c, gamma)
            fxe = func(*xe)
            if fxe < fxr:  # scelta del miglior punto tra riflessione ed espansione
                simplex[np.where((simplex == best).all(axis=1))] = xe
            else:
                simplex[np.where((simplex == best).all(axis=1))] = xr
        elif fxr > func(*second):  # caso peggiore: eseguire una contrazione
            xc = contraction(worst, c, beta)
            fxc = func(*xc)
            if fxc < func(*worst):  # se la contrazione è buona sostituire xworst con xv
                simplex[np.where((simplex == worst).all(axis=1))] = xc
            else:
                # ricostruire il simplesso
                simplex = shrink_contraction(simplex, delta)

    return best, func(*best)  # , simplexes


def h(x, i):
    if x[i] == 0:
        return 0.00025

    return 0.05


def construct_simplex(x1):
    u1, u2 = np.array([
        [1, 0],
        [0, 1]
    ])
    x2 = x1 + h(x1, 0) * u1
    x3 = x1 + h(x1, 1) * u2

    return x2, x3


def centroid(simplex):
    c = 1 / 2. * np.sum(simplex[:-1, :].T, axis=1)

    return c


def reflection(x, c, alpha):
    return c + alpha * (c - x)


def expansion(x, c, gamma):
    return c + gamma * (x - c)


def contraction(x, c, beta):
    return c + beta * (x - c)


def shrink_contraction(simplex, delta):
    best, second, worst = simplex
    newsecond = best + delta * (second - best)
    newworst = best + delta * (worst - best)

    return np.array([best, newsecond, newworst])

# functions to minimize


def booth(x, y):
    return (x + 2*y - 7)**2 + (2*x + y - 5)**2


def booth2(xs):
    x, y = xs
    return booth(x, y)


def easom(x, y):
    return -np.cos(x)*np.cos(y)*np.exp(-((x - np.pi)**2 + (y-np.pi)**2))


def easom2(xs):
    x, y = xs
    return easom(x, y)


def goldsten(x, y):
    return np.exp(0.5*(x**2 + y**2 - 25)**2) + np.power(np.sin(4*x - 3*y), 4) + 0.5*(2*x + y - 10)**2


def goldsten2(xs):
    x, y = xs
    return goldsten(x, y)


def bukin(x, y):
    return 100*np.sqrt(np.abs(y - 0.01 * x**2)) + 0.01 * np.abs(x + 10)


def bukin2(xs):
    x, y = xs
    return bukin(x, y)


vbooth = np.vectorize(booth)
veasom = np.vectorize(easom)
vgoldsten = np.vectorize(goldsten)
vbukin = np.vectorize(bukin)

x0 = [2, 2]
# print(fmin(bukin2, x0))
print(find_minimum(bukin, x0))
